## **Project Title**
IPMAN (IP Address Monitoring and Management System)

## **Getting Started**

IPMAN is the software used to monitor and manage IP Addresses of an institution distributed in different branches across the country.

## **Prerequisites**

- PHP Web Server (Apache/Nginx etc)
- MySQL Database


## **Installing**

- 1. Copy .env.example to .env (For Linux:  cp .env.example .env )
- 2. Install all dependent packages using composer (composer install)
- 3. (Linux Only) make write permission on the folder storage, bootstrap/cache, .env
- Steps 4
- _chmod 777 -R storage_
- _chmod 777 -R bootstrap/cache_
- _chmod 777 -R .env_

- 4. Create the following folders
None


- 5. Create a link to the storage directory : **php artisan storage:link**
- 6. 

## **Running the tests**


## **Deployment**



## **Built With**

- Laravel - Laravel PHP Framework.  (Current ver 5.4)
- Composer - PHP Dependency Management.
- MySQL - Database Management System.

## **Contributing**

Not Applied.

## **Version Control**

We use Git for version control. For the versions available, see the tags on this repository.

## **Authors**

- [Erick Chrysostom](gwanchi@gmail.com)
- [Martin Luhanjo](martin.luhanjo@gmail.com)
- [Allen Malibate](allentelesphory@gmail.com)

## **License**

This project is licensed to Nextbyte ICT Solutions Limited (Tanzania).

## **Copyright**

This project work is copyrighted to Nextbyte ICT Solutions Limited (Tanzania).

## **Acknowledgments**

Thanks to Antony Rappa and boilerplate Team with useful open-source Laravel-boilerplate for advance laravel code references.

## **Similar Work**


## Design Template Used

Backend - Responsive Bootstrap 4 Admin Dashboard

Current VERSION 1.0.3

## Template Url
_Add this in front of root url_

**public/template/admin_left_top_menu/default/admin_default/**

_[To access web url](https://almsaeedstudio.com/)_

## Supervisor Sample Configuration

Change the memory limit in php.ini file
memory_limit = 1024M
memory_limit = 512M
memory_limit = 128M

[program:ipman_worker_start]
process_name=%(program_name)s_%(process_num)02d
command=php /path-to-your-project/artisan  queue:work  --sleep=3 --tries=1
autostart=true
autorestart=true
user={user}
numprocs=8
redirect_stderr=true
stdout_logfile=/var/log/supervisor/worker.log

## For Task Scheduling for background processes
For **CentOS** : Make sure that **_cronie & cronie-anacron_** is installed by issuing the following command 
_`rpm -q cronie cronie-anacron`_

Edit file /etc/crontab, add the following entry

_* * * * * {user} php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1_

